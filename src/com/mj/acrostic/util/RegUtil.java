package com.mj.acrostic.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 则表达式的公用方法
 * @author zhaominglei
 * @date 2015-2-2
 * 
 */
public class RegUtil {
	
		
	/**
	 * @param value
	 * @return 根据正则表达式出匹配的字符串(取出第一列)
	 */
	public static String getMatchRegStr(String value,String reg){
		if(isBlank(value)){
			return value;
		}
		Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE
				| Pattern.DOTALL);
	    Matcher matcher = pattern.matcher(value);
	    String result = "";
	    if(matcher.find()){
	    	result = matcher.group(1);
	    }
		return result;
	}

	/**
	 * @param value
	 * @return 根据正则表达式出匹配的字符串列表
	 */
	public static List<String> getMatchRegStrs(String value,String reg){
		List<String> list = new ArrayList<String>();
		if(isBlank(value)){
			return list;
		}
		Pattern pattern = Pattern.compile(reg);
	    Matcher matcher = pattern.matcher(value);
	    String result;
	    while (matcher.find()){
	    	int i = 1;
	    	result = matcher.group(i);
	    	list.add(result);
	    	i++;
	    }
		return list;
	}	
	/**
	 * 判断字符串是否为“”或null
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str) {
		if (str == null || str.equals("")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *  过滤html中<script></script>
	 * @param str
	 * @return 
	 */
	public static String replaceScript(String str){
		String tmp = str;
		tmp = tmp.replaceAll("<script[\\s\\S]+</script *>", "");
		return tmp;
	}	
	
	/** 
     * 手机号验证 
     *  
     * @param  str 
     * @return 验证通过返回true 
     */  
    public static boolean isMobile(String str) {   
        Pattern p = null;  
        Matcher m = null;  
        boolean b = false;   
        p = Pattern.compile("^[1][3,4,5,8][0-9]{9}$"); // 验证手机号  
        m = p.matcher(str);  
        b = m.matches();   
        return b;  
    }
}
