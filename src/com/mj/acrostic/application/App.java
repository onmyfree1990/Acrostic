package com.mj.acrostic.application;

import android.app.Application;

/**
 * 全局Application
 * @author zhaominglei
 * @date 2014-12-2
 * 
 */
public class App extends Application {
	public static App app;
	
	@Override
	public void onCreate() {
		app = this;
		super.onCreate();
	}

}
